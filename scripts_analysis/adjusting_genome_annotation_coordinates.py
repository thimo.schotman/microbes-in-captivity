#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that checks all annotations to see if any are longer than the genome, and adjusts them if so.
"""

# Imports
import re
import sys
from os import listdir
from os.path import isfile, join
from multiprocessing import Pool
from itertools import repeat

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the .fna files are stored, and a directory where all the .gff files are stored.
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the .fna files and the directory with the corresponding .gff files")
        sys.exit(1)


def checking_all_gff_files(input_dir_gff):
    """Function that checks the .gff files if any annotations are longer than the genome length

    input_dir_gff: Input directory where all the .gff files are stored

    Output is written to a file that contains statistics on which annotations are too long
    """
    files = [f for f in listdir(input_dir_gff) if isfile(join(input_dir_gff, f))]
    with open("genomes_plasmids_too_long.txt", 'w') as plasmid_output:
        for file in files:
            name = file.strip().split(".gff")[0]
            genome_lengths = {}
            gff_lengths = {}
            counter = 0

            with open(f"{input_dir_gff}/{file}", 'r') as gff_input:
                for line in gff_input:
                    if re.match(rf"##sequence-region", line):
                        counter += 1
                    elif line.startswith("#"):
                        continue
                    elif line.split("\t")[2] == "region":
                        genome_lengths.update({f"{counter}": int(line.strip().split("\t")[4])})
                    else:
                        gff_lengths.update({f"{counter}": int(line.strip().split("\t")[4])})
                gff_input.close()

            writer = False

            for i in range(1, len(genome_lengths) + 1):
                length_genome = genome_lengths[f"{i}"]
                feature_length = gff_lengths[f"{i}"]

                if length_genome < feature_length:
                    if not writer and i == len(genome_lengths):
                        plasmid_output.write(f"{name},{i}:{feature_length - length_genome}\n")
                        writer = True
                    elif not writer:
                        plasmid_output.write(f"{name},{i}:{feature_length-length_genome}")
                        writer = True
                    elif i == len(genome_lengths):
                        plasmid_output.write(f",{i}:{feature_length - length_genome}\n")
                    else:
                        plasmid_output.write(f",{i}:{feature_length-length_genome}")
                elif writer and i == len(genome_lengths):
                    plasmid_output.write("\n")
                else:
                    continue

    return


def changing_coordinates(input_dir_fna, input_dir_gff, coord_line):
    """Function that adjusts the coordinates so no annotations are longer than the genome. It also adjusts the
    corresponding nucleotides in the genomes

    input_dir_fna: The input directory where all the .fna files are stored
    input_dir_gff: The input directory where all the .gff files are stored
    coord_line: a line which from the file with annotations that are too long. It contains the name of the annotation,
    which part of the genome (which plasmid or the main genome), and how much longer the annotation is than the genome

    It immediately writes the adjusted genomes and annotations to new files
    """

    coordinates = {}
    headers = {}
    sequences = {}
    counter = 0
    name = coord_line.strip().split(",")[0]

    for coordinate in range(len(coord_line.split(","))):
        if coordinate == 0:
            continue
        else:
            sequence_id = coord_line.strip().split(",")[coordinate].split(":")[0]
            difference = int(coord_line.strip().split(",")[coordinate].split(":")[1])
            coordinates.update({f"{sequence_id}": difference})

    fna_file = "{}{}.fna".format(input_dir_fna, name)
    gff_file = "{}{}.gff".format(input_dir_gff, name)

    with open(fna_file, 'r') as fna_input:
        for line_fna_input in fna_input:
            if line_fna_input.startswith(">"):
                counter += 1
                headers.update({f"{counter}": line_fna_input})
                sequences.update({f"{counter}": ""})
            else:
                old_sequence = sequences[f'{counter}']
                new_sequence = old_sequence + line_fna_input.strip()
                sequences.update({f"{counter}": new_sequence})
        fna_input.close()

    for index, diff in coordinates.items():
        sequence = sequences[index]
        diff_nuc = sequence[:diff]
        rest_nuc = sequence[diff:]
        new_nuc = rest_nuc + diff_nuc
        sequences.update({index: new_nuc})

        with open(fna_file, 'w') as fna_output:
            for nz in range(1, len(sequences) + 1):
                fna_output.write(headers[f"{nz}"])
                sequence_to_write = sequences[f"{nz}"]

                for j in range(80, len(sequence_to_write), 80):
                    if j == 80:
                        fna_output_line = sequence_to_write[:j]
                    else:
                        fna_output_line = sequence_to_write[j-80:j]
                    fna_output.write(f"{fna_output_line}\n")

                if len(sequence_to_write) % 80 != 0:
                    remainder = len(sequence_to_write) % 80
                    last_nucleotides = sequence_to_write[len(sequence_to_write)-remainder:]
                    fna_output.write(f"{last_nucleotides}\n")
            fna_output.close()

    with open(gff_file, 'r') as gff_input:
        region_gff = []
        region_counter = 0
        for gff_input_line in gff_input:
            if gff_input_line.startswith("#"):
                region_gff.append(gff_input_line)
            elif gff_input_line.split("\t")[2] == "region":
                region_counter += 1
                region_gff.append(gff_input_line)
            else:
                if str(region_counter) in coordinates:

                    line_elements = gff_input_line.split("\t")
                    if int(line_elements[4]) - coordinates[f"{region_counter}"] <= 0:
                        raise Exception(f"Adjusting the first feature in file {name} would result in the "
                                        f"loss of the first feature.")
                    elif int(line_elements[3]) - coordinates[f"{region_counter}"] <= 0:
                        line_elements[3] = 1
                        line_elements[4] = int(line_elements[4]) - coordinates[f"{region_counter}"]

                    else:
                        line_elements[3] = int(line_elements[3]) - coordinates[f"{region_counter}"]
                        line_elements[4] = int(line_elements[4]) - coordinates[f"{region_counter}"]

                    new_line = ""
                    for entry in line_elements:
                        if entry == line_elements[-1]:
                            new_line += "{}".format(entry)
                        else:
                            new_line += "{}\t".format(entry)
                    region_gff.append(new_line)
                else:
                    region_gff.append(gff_input_line)
    gff_input.close()

    with open(gff_file, 'w') as gff_output:
        for line_gff_region in region_gff:
            gff_output.write(line_gff_region)
    gff_output.close()

    return


def main():
    """The main function that runs all other functions and parses the file with the genomes that are too long. It
    runs the changing_coordinates function in parallel.
    """
    user_input = catch_input()
    checking_all_gff_files(user_input[1])
    lines = []
    with open("genomes_plasmids_too_long.txt") as genome_input:
        for line in genome_input:
            lines.append(line)
        genome_input.close()

    with Pool(4) as pool:
        results = pool.starmap(changing_coordinates, zip(repeat(user_input[0]), repeat(user_input[1]), lines))

# Main


if __name__ == '__main__':
    main()



