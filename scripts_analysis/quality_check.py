#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs the quality check for the cocultures (and monocultures)
"""

# Imports
import sys
from os import listdir
from os.path import isfile, join
import subprocess
import re

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory in which the untrimmed files are stored, the directory in which the trimmed files are to be
    stored, the directory with the files for the quality check, and the file with the metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3]), str(sys.argv[4])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the untrimmed files, the directory to store the trimmed "
              "files, the directory for the quality check and the file with the metadata")
        sys.exit(1)


def input_files(input_dir):
    """Function that extracts all files from an input directory

    input_dir: the directory from which to extract all files

    output: a list of files for a give directory
    """
    files = [f for f in listdir(input_dir) if isfile(join(input_dir, f))]
    return files


def read_meta_data(file_meta_data):
    """Function that reads the metadata from the file with the metadata

    file_meta_data: The metadata file to read from

    output: list of lines from the metadata
    """
    file_1 = open(file_meta_data, 'r')
    raw_lines = file_1.readlines()
    lines = []
    for line in raw_lines:
        line = line.strip()
        lines.append(line.split(","))

    return lines


def single_dual_strand_divider(list_of_input_files, meta_lines):
    """Function that divides the experiments in single strand experiments and dual strand experiments

    list_of_input_files: list of files that need to be checked for being single or dual strand files
    meta_lines: the lines from the metadata

    output:
        single_strands: list of files that need to treated as single strand experiments
        dual_strands: list of files that need to be treated as dual strand experiments
    """
    single_strands = []
    dual_strands = []

    for i in range(len(meta_lines)):
        if len(meta_lines[i]) > 2:
            for j in range(len(list_of_input_files)):
                file_1 = re.findall(rf"{meta_lines[i][0]}[._].*", list_of_input_files[j])
                file_2 = re.findall(rf"{meta_lines[i][1]}[._].*", list_of_input_files[j])
                if len(file_1) > 0:
                    dual_strands.append(file_1[0])
                elif len(file_2) > 0:
                    dual_strands.append(file_2[0])
        else:
            for j in range(len(list_of_input_files)):
                file_1 = re.findall(rf"{meta_lines[i][0]}[._].*", list_of_input_files[j])
                if len(file_1) > 0:
                    single_strands.append(file_1[0])

    return single_strands, dual_strands


def running_trim_galore(input_dir, untrimmed_files, output_folder, meta_lines):
    """Function that runs trim-galore

    input_dir: the input directory with the untrimmed files
    untrimmed_files: list of untrimmed files that need to undergo trimming
    output_folder: the folder in which to store the output of the trimming
    meta_lines: the lines from the metadata
    """
    files_single_strands, files_dual_strands = single_dual_strand_divider(untrimmed_files, meta_lines)
    for single_strands in files_single_strands:
        cmd = "trim_galore --output_dir {} --cores 8 {}{}".format(output_folder, input_dir, single_strands)
        run = subprocess.run(cmd, shell=True)

    for i in range(0, len(files_dual_strands), 2):
        cmd = "trim_galore --output_dir {} --cores 8 --paired {}{} {}{}".format(output_folder, input_dir,
                                                                                files_dual_strands[i], input_dir,
                                                                                files_dual_strands[i+1])
        run = subprocess.run(cmd, shell=True)

    return


def removing_txt_files(list_of_qc_files):
    """Function that removes the text files from the list of files that need to undergo quality control

    list_of_qc_files: list of files from the directory in which the results from the trimming are stored

    output: list of files that are the result of the trimming, but without any text files present in the list
    """
    for file in list_of_qc_files:
        if '.txt' in file:
            list_of_qc_files.remove(file)
        else:
            continue
    return list_of_qc_files


def running_quality_check(files_for_qc, folder_trimmed_fq, output_dir):
    """Function that runs FastQC

    files_for_qc: list of files that need to undergo quality checking
    folder_trimmed_fq: the folder with the trimmed files
    output_dir: the output directory to which to write the results of the quality check
    """
    for i in range(len(files_for_qc)):
        cmd = "fastqc --outdir {} --threads 8 {}{}".format(output_dir, folder_trimmed_fq, files_for_qc[i])
        run = subprocess.run(cmd, shell=True)

    return

# Main


if __name__ == "__main__":
    directories = catch_input()
    metadata = read_meta_data(directories[3])
    list_input_files = input_files(directories[0])
    running_trim_galore(directories[0], list_input_files, directories[1], metadata)
    list_files_qc = input_files(directories[1])
    final_qc_files = removing_txt_files(list_files_qc)
    running_quality_check(final_qc_files, directories[1], directories[2])

