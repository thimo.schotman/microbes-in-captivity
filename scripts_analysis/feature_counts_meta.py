#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs feature counts for the metatranscriptomic data
"""

# Imports
import sys
import subprocess
from os import listdir
from os.path import isfile, isdir, join
from multiprocessing import Pool
from itertools import repeat

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the main directory where all the data for the metatranscriptomic experiments is stored, the name of file
    that contains the pantools gff list, and the pangenome metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3])]
        return input_dirs
    except IndexError:
        print("Please give in the main directory, the pantools gff list, and the pangenome metadata")
        sys.exit(1)


def meta_library_creator(metadata):
    """Function that creates a dictionary containing the options with which to run featureCounts

    metadata: the name of the metadata file

    output: a dictionary that has the accession as key and the options for each accession as value
    """
    metadata_library = {}
    with open(f"{metadata}", 'r') as metadata:
        for line in metadata:
            if len(line.strip().split(",")) > 2:
                options = " -p -B -C -P"
                srr = line.strip().split(",")[0].split("_")[0]
            else:
                options = ""
                srr = line.strip().split(",")[0]
            s = line.strip().split(",")[-1]

            metadata_library.update({srr: {"options": options, "s": s}})
    metadata.close()
    return metadata_library


def number_genome_matching(pan_gff_list):
    """Function that matches the accession to the number of the gff file

    pan_gff_list: name of the file that contains the pantools gff list

    output: a dictionary with the number of each gff as key, and the path to the corresponding gff file as value
    """
    gff_number = {}
    with open(pan_gff_list, 'r') as gff_input:
        for line in gff_input:
            gff_number.update({line.strip().split()[0]: line.strip().split()[1]})
    gff_input.close()

    return gff_number


def running_feature_counts(metadata_lib, main_dir, srr_dir, gff_number):
    """Function that runs featureCounts

    metadata_lib: the metadata dictionary that contains the options for each accession
    main_dir: the main directory where all the data for the metatranscriptomic experiments is stored
    srr_dir: the specific accession directory in which to find the mapped reads
    gff_number: the dictionary that contains the gff number and the corresponding path to the gff file
    """
    output_dir = f"{main_dir}{srr_dir}/counts"
    input_dir = f"{main_dir}{srr_dir}/sorted_reads"
    cmd_output_dir = f"mkdir {output_dir}"
    run_output_dir = subprocess.check_call(cmd_output_dir, shell=True)

    files = [f for f in listdir(input_dir) if isfile(join(input_dir, f))]
    for file in files:
        number = file.strip().split("_")[1]

        cmd = f"featureCounts{metadata_lib[srr_dir]['options']} --fracOverlap 0.5 -T 4 {metadata_lib[srr_dir]['s']} " \
              f"-a {gff_number[number]} -t CDS -g locus_tag -o {output_dir}/{number}_counts.txt {input_dir}/{file}"
        run = subprocess.check_call(cmd, shell=True)

    return


def main():
    """Main function that runs all other functions. It runs running_feature_counts in parallel"""
    user_input = catch_input()
    metadata_lib = meta_library_creator(user_input[2])
    gff_number_link = number_genome_matching(user_input[1])
    srr_dirs = [f for f in listdir(user_input[0]) if isdir(join(user_input[0], f))]
    srr_dirs.remove("all_read_directions")
    srr_dirs.remove("summaries")
    srr_dirs.remove("unmapped")

    with Pool(4) as pool:
        results = pool.starmap(running_feature_counts, zip(repeat(metadata_lib), repeat(user_input[0]), srr_dirs,
                                                           repeat(gff_number_link)))
    return

# Main


if __name__ == '__main__':
    main()
