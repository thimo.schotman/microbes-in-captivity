#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs RSEQC to infer the read direction of the experiments
"""

# Imports
import re
import sys
import subprocess

# Functions


def catch_input():

    """Catches the user input from the command line

    returns the directory where the mapped reads are stored, and the file with the metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the main directory where the mapped reads are stored, and the file with the metadata")
        sys.exit(1)


def read_meta_data(file_meta_data):
    """Function that reads the metadata. By now you're probably wondering why I haven't made a separate file so
    I could import this function, and the simple truth is that I'm not sure why I didn't do that. It would have been
    so much easier. Oh well, it is what it is I guess

    file_meta_data: The file with the metadata

    output: list of lines from the metadata in which the underscore has been removed in the identifiers
    """
    file_1 = open(file_meta_data, 'r')
    raw_lines = file_1.readlines()
    lines = []
    for line in raw_lines:
        line = line.strip()
        lines.append(line.split(","))

    clean_lines = clean_identifier(lines)
    return clean_lines


def clean_identifier(lines):
    """Function that removes the underscore from the identifiers in the metadata. If this function seems familiar,
    it is because this one too should have been in a separate file so I could have imported it every time I needed it

    lines: list of lines from the metadata

    output: list of lines from the metadata without underscores in the identifiers
    """
    for i in range(len(lines)):
        if re.match(rf".*_.*", lines[i][0]):
            identifier = re.split(rf"_", lines[i][0])[0]
            lines[i][0] = identifier

    return lines


def getting_read_direction(clean_lines, main_dir):
    """Function that runs the infer_experiment script

    clean_lines: the lines from the metadata without underscores in the identifiers
    main_dir: the main directory in which all the information for the class of experiments (mono/coculture) is stored
    """
    input_dir = f"{main_dir}mapped_reads/"
    output_path = f"{main_dir}read_direction/"
    cmd_output_path = f"mkdir {output_path}"
    create_output_folder = subprocess.check_call(cmd_output_path, shell=True)

    for line in clean_lines:
        strain = line[-1]
        cmd_run_infer = f"infer_experiment.py -r genome_bed/{strain}.bed -i {input_dir}{line[0]}_sorted.sam > " \
                        f"{output_path}{line[0]}_read_direction.txt"
        run_infer = subprocess.check_call(cmd_run_infer, shell=True)

    return


def adding_read_dir_to_metadata(main_dir, clean_lines, file_meta_data):
    """Function that goes through the result of the infer_experiment script and automatically adds it to the metadata

    main_dir: the main directory in which all the information for the class of experiments (mono/coculture) is stored
    clean_lines: the lines from the metadata without the underscores in the identifiers
    file_meta_data: the name of the file with the metadata
    """

    for i in range(len(clean_lines)):
        read_file = f"{main_dir}read_direction/{clean_lines[i][0]}_read_direction.txt"
        with open(read_file, "r") as open_read_file:
            lines = open_read_file.readlines()

            direction_1 = float(lines[4].split(":")[1])
            direction_2 = float(lines[5].split(":")[1])

            if direction_1 > 2 * direction_2:
                fc_setting = "-s 1"
            elif direction_2 > 2 * direction_1:
                fc_setting = "-s 2"
            else:
                fc_setting = "-s 0"

        open_read_file.close()

        with open(file_meta_data, "r+") as metadata_file:
            metadata_lines = metadata_file.readlines()
            addition = f",{fc_setting}\n"
            metadata_lines[i] = metadata_lines[i].replace("\n", "") + addition

        with open(file_meta_data, "w+") as new_metadata_file:
            for line in metadata_lines:
                new_metadata_file.write(line)

        metadata_file.close()

    return

# Main


if __name__ == "__main__":
    user_input = catch_input()
    metadata = read_meta_data(user_input[1])
    getting_read_direction(metadata, user_input[0])
    adding_read_dir_to_metadata(user_input[0], metadata, user_input[1])
