#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that takes the PanTools homology grouping, checks all the count files produced by PanTools, and traces everything
back to MG1655 identifiers through the homology groups
"""

# Imports
import re
import sys
from os import listdir
from os.path import isfile, isdir, join
from multiprocessing import Pool
from itertools import repeat
import subprocess

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the data for the metatranscriptomic analysis is stored and the name of the pantools
    homology file
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the main directory where all the metatranscriptomic data is stored, and the pantools"
              "homology file")
        sys.exit(1)


def creating_mg1655_dict(input_dir):
    """Function that creates a dictionary with all the mg1655 identifiers and their starting values based on the count
    file that has been created for the mg1655 genome through pantools

    input_dir: the input directory where all the count files are stored for a specific accession

    ouput: dictionary that has the mg1655 identifier as key, and the corresponding count value as value
    """
    mg1655_dict = {}
    with open(f"{input_dir}1_counts.txt", 'r') as dict_input:
        for line in dict_input:
            if line.startswith("#"):
                continue
            elif line.startswith("Geneid"):
                continue
            else:
                identifier = line.strip().split("\t")[0]
                value = int(line.strip().split("\t")[6])
                mg1655_dict.update({identifier: value})

    return mg1655_dict


def creating_homology_dict(homology_file):
    """Function that creates a dictionary that contains all the homology groups that contain mg1655 identifiers and all
    the respective members of each group

    homology_file: the name of the PanTools homology grouping file

    output: dictionary that has as key the mg1655 identifier found in a homology group, and as value a list with all
    the respective members of said homology group
    """
    homology_dict = {}
    with open(homology_file, 'r') as homology_input:
        for line in homology_input:
            if re.search(rf"b[0-9]{4}", line):
                header = True
                entries_clean = []
                entries = line.strip().split()
                for entry in entries:
                    if header:
                        header = False
                    elif re.search(rf"b[0-9]{4}", entry):
                        identifier = entry.split("-")[1]
                        entries.remove(entry)
                    else:
                        entry_clean = entry.split("-")[1]
                        entries_clean.append(entry_clean)
                homology_dict.update({identifier: entries_clean})
            else:
                continue
        homology_input.close()

    return homology_dict


def count_homology_converter(input_dir, main_dir, homology_dict):
    """Function that converts all the counts from the count files to mg1655 identifiers so long as they appear in
    the homology groups

    input_dir: the input directory where all the count files for a specific experiment are stored
    main_dir: the main directory where all the data for the metatranscriptomic experiments is stored
    homology_dict: the homology dictionary
    """
    count_path = f"{main_dir}{input_dir}/counts/count_files/"
    files = [f for f in listdir(count_path) if isfile(join(count_path, f))]
    mg1655_dict = creating_mg1655_dict(count_path)
    for file in files:
        if file == "1_counts.txt":
            continue
        else:
            with open(f"{count_path}{file}", 'r') as count_input:
                for line in count_input:
                    if line.startswith("#"):
                        continue
                    elif line.startswith("Geneid"):
                        continue
                    elif line.split("\t")[6] == 0:
                        continue
                    else:
                        identifier = line.strip().split("\t")[0]
                        for entry in homology_dict:
                            if identifier in homology_dict[entry]:
                                value = mg1655_dict[entry] + int(line.strip().split("\t")[6])
                                mg1655_dict.update({entry: value})
                            else:
                                continue
    start_mg1655_file = f"{count_path}1_counts.txt"
    srr = input_dir
    writing_output(main_dir, srr, start_mg1655_file, mg1655_dict)

    return


def writing_output(main_dir, srr, start_mg1655_file, updated_mg1655_dict):
    """Function that writes the updated mg1655 dictionary to a single new file

    main_dir: the main directory where all the data for the metatranscriptomic experiments is stored
    srr: the accession for which to write the updated mg1655 dictionary
    start_mg1655_file: the file for genome one in the pangenome which is the mg1655 genome. This file will be used as a
        template
    updated_mg1655_dict: the updated mg1655 dictionary which now contains all counts from all files, insofar they can
    be traced back
    """
    with open(f"{main_dir}/homology_counts/{srr}_counts_homology.txt", 'w') as homology_counts:
        with open(start_mg1655_file, 'r') as template:
            for line in template:
                if line.startswith("#"):
                    continue
                elif line.startswith("Geneid"):
                    homology_counts.write(line)
                else:
                    info = line.split("\t")
                    value = updated_mg1655_dict[info[0]]
                    homology_counts.write(f"{info[0]}\t{info[1]}\t{info[2]}\t{info[3]}\t{info[4]}\t{info[5]}\t"
                                          f"{value}\n")

    return


def main():
    """Main function that runs all the functions. It makes sure some folders with accessory data are removed first.
    The count_homology_converter function is run in parallel.
    """
    user_input = catch_input()
    homology_dictionary = creating_homology_dict(user_input[1])
    srr_folders = [f for f in listdir(user_input[0]) if isdir(join(user_input[0], f))]
    srr_folders.remove('unmapped')
    srr_folders.remove('summaries')
    srr_folders.remove("all_read_directions")

    cmd_homology_count_folder = f"mkdir {user_input[0]}homology_counts"
    create_homology_count_folder = subprocess.check_call(cmd_homology_count_folder, shell=True)

    with Pool(6) as pool:
        run_homology_converter = pool.starmap(count_homology_converter, zip(srr_folders, repeat(user_input[0]),
                                                                            repeat(homology_dictionary)))

    return

# Main


if __name__ == '__main__':
    main()
