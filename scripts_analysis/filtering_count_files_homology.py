#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that filters count files so that only those counts that are in the homology grouping are present
"""

# Imports
import sys
import re
from os import listdir
from os.path import isfile, join

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory where all the count files are stored, the output directory to which you want to write the
    filtered count files, and the name of the pantools homology list
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the count files, the output directory, and the pantools homology list")
        sys.exit(1)


def creating_homology_set(homology_file):
    """Function that creates a set of mg1655 identifiers that are present in the homology groups

    homology_file: file with the homology grouping

    output: a set with mg1655 identifiers that are present in the homology grouping
    """
    homology_set = set()
    with open(homology_file, 'r') as homology_input:
        for line in homology_input:
            if re.search(rf"b[0-9]{4}", line):
                entries = line.strip().split()
                for entry in entries:
                    if re.search(rf"b[0-9]{4}", entry):
                        identifier = entry.split("-")[1]
                        homology_set.add(identifier)
                    else:
                        continue
            else:
                continue
        homology_input.close()

    return homology_set


def filtering_count_file(input_dir, output_dir, homology_set):
    """Function that filters the count files

    input_dir: the directory where all the count files are stored
    output_dir: the directory to which to write the filtered count files
    homology_set: the set of mg1655 identifiers that are present in the homology groups
    """

    files = [f for f in listdir(input_dir) if isfile(join(input_dir, f))]

    for file in files:
        with open(f"{output_dir}filtered_{file}", 'w') as filtered_counts:
            with open(f"{input_dir}{file}", 'r') as raw_counts:
                for line in raw_counts:
                    if line.startswith("Geneid"):
                        filtered_counts.write(line)
                    elif line.strip().split()[0] in homology_set:
                        filtered_counts.write(line)
                    else:
                        continue
            raw_counts.close()
        filtered_counts.close()

    return


def main():
    """Main function that runs all the other functions. Nothing special to report"""
    user_input = catch_input()
    homologies = creating_homology_set(user_input[2])
    filtering_count_file(user_input[0], user_input[1], homologies)

# Main


if __name__ == '__main__':
    main()
