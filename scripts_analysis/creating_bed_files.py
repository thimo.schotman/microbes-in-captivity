#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that creates the necessary .bed files to run the infer_experiment script
"""

# Imports
import sys
import subprocess
from os import listdir
from os.path import isfile, join

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory with the gff files, and the directory in which to store the bed files
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the GFF files and the directory in which to store the bed files")
        sys.exit(1)


def creating_bed_files(input_dir_gff, output_dir_bed):
    """Function that runs the command to create the bed files

    input_dir_gff: the input directory with the gff files
    output_dir_bed: the output directory in which to store the .bed files
    """
    files = [f for f in listdir(input_dir_gff) if isfile(join(input_dir_gff, f))]

    for file in files:
        name = file.split("_genomic.gff")[0]

        cmd = f"gff2bed <{input_dir_gff}{file}> {output_dir_bed}{name}.gff.bed"
        run = subprocess.run(cmd, shell=True)

    return

# Main


if __name__ == '__main__':
    user_input = catch_input()
    creating_bed_files(user_input[0], user_input[1])
