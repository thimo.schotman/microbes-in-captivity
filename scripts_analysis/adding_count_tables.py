#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that takes all count files resulting from the PanTools mapping and adds up all the counts
"""

# Imports
import sys
from os import listdir
from os.path import isfile, join

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the count files are stored and a name to use for the output file
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the count files to add up, and the name of the output file")
        sys.exit(1)


def adding_count_files(input_dir, output_file):
    """Adds up the counts from all the files and puts them in a single file

    input_dir: The input directory where all the count files to be added up are stored
    output_file: The name to be used for the output file in which the function returns the added counts
    """
    count_dict = {}

    files = [f for f in listdir(input_dir) if isfile(join(input_dir, f))]
    first = True

    for file in files:
        with open(f"{input_dir}{file}", 'r') as counts_in:
            if first:
                for count, line in enumerate(counts_in):
                    if line.startswith("Geneid"):
                        continue
                    else:
                        count_dict.update({count: int(line.strip().split()[6])})
                first = False
            else:
                for count, line in enumerate(counts_in):
                    if line.startswith("Geneid"):
                        continue
                    else:
                        added_value = count_dict[count] + int(line.strip().split()[6])
                        count_dict.update({count: added_value})

        counts_in.close()

    with open(f"{input_dir}{output_file}", 'w') as counts_out:
        with open(f"{input_dir}{files[0]}", 'r') as counts_format:
            for count_format, line_format in enumerate(counts_format):
                if line_format.startswith("Geneid"):
                    counts_out.write(line_format)
                else:
                    rest_line = line_format.strip().split()
                    counts_out.write(f"{rest_line[0]}\t{rest_line[1]}\t{rest_line[2]}\t{rest_line[3]}\t"
                                     f"{rest_line[4]}\t{rest_line[5]}\t{count_dict[count_format]}\n")
        counts_format.close()
    counts_out.close()

    return


def main():
    """The main function that runs all the other functions. Nothing special here."""
    user_input = catch_input()
    adding_count_files(user_input[0], user_input[1])

    return

# Main


if __name__ == '__main__':
    main()
