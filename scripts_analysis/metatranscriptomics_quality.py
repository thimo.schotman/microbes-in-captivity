#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs the trim-galore and FastQC for the metatranscriptomic data
"""

# Imports
import sys
from os import listdir
from os.path import isfile, join
import subprocess

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory with the untrimmed file, the directory to store the trimmed files, the directory to store
    the quality check files, and the file with the metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3]), str(sys.argv[4])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the untrimmed files, the directory to store the trimmed "
              "files, the directory for the quality check and the file with the metadata")
        sys.exit(1)


def running_trim_galore(input_dir, output_dir, meta_file):
    """Function that runs trim-galore

    input_dir: the input directory where all the untrimmed files are stored
    output_dir: the output directory to which to write the results
    meta_file: the name of the file with the metadata
    """
    with open(meta_file, 'r') as input_meta:
        for line in input_meta:
            if len(line.strip().split(",")) > 1:
                end_1 = line.strip().split(",")[0]
                end_2 = line.strip().split(",")[1]
                cmd = "trim_galore --output_dir {} --cores 8 --paired {}{}.fastq {}{}.fastq".format(output_dir,
                                                                                                    input_dir, end_1,
                                                                                                    input_dir, end_2)
                run = subprocess.run(cmd, shell=True)
            else:
                unpaired = line.strip()
                cmd = "trim_galore --output_dir {} --cores 8 {}{}.fastq".format(output_dir, input_dir, unpaired)
                run = subprocess.run(cmd, shell=True)

    return


def running_fastqc(input_dir_qc, output_dir_qc):
    """Function that runs FastQC

    input_dir_qc: input directory that contains the files over which FastQC should be ran
    output_dir_qc: the output directory in which to store the results of the quality check
    """
    files = [f for f in listdir(input_dir_qc) if isfile(join(input_dir_qc, f))]
    for file in files:
        if '.txt' in file:
            continue
        else:
            cmd = "fastqc --outdir {} --threads 8 {}{}".format(output_dir_qc, input_dir_qc, file)
            run = subprocess.run(cmd, shell=True)

    return

# Main


if __name__ == '__main__':
    user_input = catch_input()
    running_trim_galore(user_input[0], user_input[1], user_input[3])
    running_fastqc(user_input[1], user_input[2])
