#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs PanTools for the verification step with PRECISE data
"""

# Imports
import sys
import subprocess

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory in which the fastq files are stored, the directory with the pangenome, the pantools genome
    list, and the file with the metadata for the verification
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3]), str(sys.argv[4])]
        return input_dirs
    except IndexError:
        print("Please give in the directory in which the fastq files are stored, the directory with the pangenome, "
              "the pantools genome list, and the meta-data file for the meta-transcriptomic reads")
        sys.exit(1)


def creating_folders(meta_file):
    """Function that creates all the necessary folders to store the outputs of the script in

    meta_file: the file with metadata for the verification
    """
    cmd_general = "mkdir pantools_verification_single"
    run = subprocess.check_call(cmd_general, shell=True)

    with open(meta_file, 'r') as meta_input:
        for line in meta_input:
            if len(line.split(",")) == 2:
                srr = line.strip().split(",")[0].split("_")[0]
                cmd_srr = f"mkdir pantools_verification_single/{srr}"
                run = subprocess.run(cmd_srr, shell=True)
                cmd_mapped_reads = f"mkdir pantools_verification_single/{srr}/mapped_reads"
                run = subprocess.run(cmd_mapped_reads, shell=True)
            else:
                srr = line.strip()
                cmd_srr = f"mkdir pantools_verification_single/{srr}"
                run = subprocess.run(cmd_srr, shell=True)
                cmd_mapped_reads = f"mkdir pantools_verification_single/{srr}/mapped_reads"
                run = subprocess.run(cmd_mapped_reads, shell=True)
    meta_input.close()

    return


def running_mapping(input_fastq, pangenome_dir, genome_list, meta_file_line):
    """Function that actually runs the mapping using PanTools

    input_fastq: the input directory where all the fastq files are stored
    pangenome_dir: the directory in which the pangenome is kept
    genome_list: the file with the genomes to map against with PanTools
    meta_file_line: a line from the file with the metadata
    """
    if len(meta_file_line.split(",")) == 2:
        srr = meta_file_line.strip().split(",")[0].split("_")[0]
        cmd_pantools = f"java -jar /lustre/BIF/nobackup/schot042/tools/pantools/target/pantools-3.5.jar map " \
                       f"-o pantools_verification_single/{srr}/mapped_reads --best-hits=random " \
                       f"--out-format=SAM --interleaved --sensitivity=very-fast {pangenome_dir} " \
                       f"{genome_list} {input_fastq}{srr}_1_val_2.fq.gz {input_fastq}{srr}_2_val_1.fq.gz"
        run = subprocess.run(cmd_pantools, shell=True)
    else:
        srr = meta_file_line.strip()
        cmd_pantools = f"java -jar /lustre/BIF/nobackup/schot042/tools/pantools/target/pantools-3.5.jar map " \
                       f"-o pantools_verification_single/{srr}/mapped_reads --best-hits=random " \
                       f"--out-format=SAM --sensitivity=very-fast {pangenome_dir} {genome_list} " \
                       f"{input_fastq}{srr}_trimmed.fq.gz"
        run = subprocess.run(cmd_pantools, shell=True)

    return


def main():
    """Main function that runs all other functions. The mapping cannot be ran in parallel, because PanTools does not
    allow for that"""
    user_input = catch_input()
    list_lines = list(open(user_input[3]).readlines())
    creating_folders(user_input[3])

    for line in list_lines:
        running_mapping(user_input[0], user_input[1], user_input[2], line)

    return

# Main


if __name__ == '__main__':
    main()
