#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that takes a raw count table with all the genes that appear in the homology grouping and converts all the counts
to TPM counts.
"""


# Imports
import sys
import re
import pandas as pd
import math
from os import listdir
from os.path import isfile, join
from multiprocessing import Pool
from itertools import repeat

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the count tables are stored, the output directory to write to, a count file to extract
    the length of all genes, and the name of the pantools homology file
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3]), str(sys.argv[4])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the count tables, the output directory, a count file to extract "
              "the lengths from, and the pantools homology list")
        sys.exit(1)


def creating_homology_set(homology_file):
    """Function that creates a set of mg1655 that are all present in the homology groupings

    homology_file: the name of the PanTools homology file

    output: a set of identifiers of genes that are the mg1655 genes present in the homology grouping
    """
    homology_set = set()
    with open(homology_file, 'r') as homology_input:
        for line in homology_input:
            if re.search(rf"b[0-9]{4}", line):
                entries = line.strip().split()
                for entry in entries:
                    if re.search(rf"b[0-9]{4}", entry):
                        identifier = entry.split("-")[1]
                        homology_set.add(identifier)
                    else:
                        continue
            else:
                continue
        homology_input.close()

    return homology_set


def creating_length_dict(homology_set, lengths_file):
    """Function that creates a dictionary with the length of each gene present in the homology set

    homology_set: the set of mg1655 genes present in the homology groups
    lengths_file: the counts file which contains the lengths of all the genes

    output: a dictionary that contains the mg1655 gene as a key, and the length of said gene as the value
    """
    lengths_dict = {}

    with open(f"{lengths_file}", 'r') as lengths_in:
        for line in lengths_in:
            if line.startswith("Geneid"):
                continue
            elif line.startswith("#"):
                continue
            else:
                if line.strip().split()[0] in homology_set:
                    lengths_dict.update({line.strip().split()[0]: int(line.strip().split()[5])})
                else:
                    continue
    lengths_in.close()

    return lengths_dict


def calculating_tpm(file, input_dir, output_dir, lengths_dict):
    """Function that calculates the TPM value for each count value

    file: the file from which to extract the counts
    input_dir: the directory in which this file can be found
    output_dir: the directory to which to write the output file
    lengths_dir: the dictionary that contains the length for each mg1655 gene
    """

    df = pd.read_table(f"{input_dir}{file}", sep=",", header='infer')

    df_new = pd.DataFrame(columns=df.columns, index=df.index)

    name_list = df.columns[1:len(df.columns)]
    for index in df.index:
        df_new.at[index, 'Geneid'] = df._get_value(index, 'Geneid')

    for name in name_list:
        sum_fpk = 0
        for index in df.index:
            fpk = df._get_value(index, name) / lengths_dict[df._get_value(index, 'Geneid')]
            df_new.at[index, name] = fpk
            sum_fpk += fpk

        if sum_fpk == 0:
            continue
        else:
            for index_2 in df.index:
                fpk_2 = df_new._get_value(index_2, name)
                df_new.at[index_2, name] = math.log2((10 ** 6 * (fpk_2 / sum_fpk) + 1))

    df_new.to_csv(f"{output_dir}log_tpm_{file}", sep=",", header=True, index=False)

    return


def main():
    """Main function that runs all other functions. The calculating_tpm function is ran parallel"""
    user_input = catch_input()
    homologies = creating_homology_set(user_input[3])
    lengths = creating_length_dict(homologies, user_input[2])

    files = [f for f in listdir(user_input[0]) if isfile(join(user_input[0], f))]

    with Pool(4) as pool:
        run_homology_converter = pool.starmap(calculating_tpm, zip(files, repeat(user_input[0]),
                                                                   repeat(user_input[1]),
                                                                   repeat(lengths)))

    return

# Main


if __name__ == '__main__':
    main()
