#!/usr/bin/env python3
"""
Author: Thimo Schotman
Quick and dirty script that will sort all .sam files in the directory of choice, as long as a metafile is provided
"""

# Imports
import re
import sys
import subprocess

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory in which to sort the sam files, and the file with the metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the SAM files and the file with the metadata")
        sys.exit(1)


def read_meta_data(file_meta_data):
    """Function that reads the metadata. By now you're probably wondering again why I haven't made a separate file so
    I could import this function, and the simple truth is that I'm not sure why I didn't do that. It would have been
    so much easier. Oh well, it is what it is I guess

    file_meta_data: The file with the metadata

    output: list of lines from the metadata in which the underscore has been removed in the identifiers
    """
    file_1 = open(file_meta_data, 'r')
    raw_lines = file_1.readlines()
    lines = []
    for line in raw_lines:
        line = line.strip()
        lines.append(line.split(","))

    clean_lines = clean_identifier(lines)

    return clean_lines


def clean_identifier(lines):
    """Function that removes the underscore from the identifiers in the metadata. If this function seems familiar,
    it is because this one too should have been in a separate file so I could have imported it every time I needed it

    lines: list of lines from the metadata

    output: list of lines from the metadata without underscores in the identifiers
    """
    for i in range(len(lines)):
        if re.match(rf".*_.*", lines[i][0]):
            identifier = re.split(rf"_", lines[i][0])[0]
            lines[i][0] = identifier

    return lines


def sorting_bam(clean_lines, input_directory):
    """Function that actually sorts the sam files

    clean_lines: the lines from the metadat without the underscore in the identifiers
    input_directory: the directory in which the sam files to be sorted are stored
    """
    for i in range(len(clean_lines)):
        cmd = "samtools sort -@ 4 {}{}.sam > {}{}_sorted.sam".format(input_directory, clean_lines[i][0],
                                                                     input_directory, clean_lines[i][0])
        run = subprocess.run(cmd, shell=True)

# Main


if __name__ == "__main__":
    user_input = catch_input()
    metadata = read_meta_data(user_input[1])
    sorting_bam(metadata, user_input[0])
