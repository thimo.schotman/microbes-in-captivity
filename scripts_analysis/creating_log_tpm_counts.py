#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that normalised the count files, but for all genes instead of only the genes found in the homology groups
"""

# Imports
import sys
from os import listdir
from os.path import isfile, join
from multiprocessing import Pool
from itertools import repeat
import math

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the count files are stored and the directory where all the log TPM counts are to be
    stored
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory where all the count files are stored, and the directory where you want the"
              "adjusted files to be stored")
        sys.exit(1)


def calculating_tpm(input_dir, input_file, output_dir):
    """Function that calculates the log TPM value for each count value and writes that to a new file

    input_dir: the input directory where all the count files can be found
    input_file: the input file from which to take the counts
    output_dir: the output directory which to write the resulting files to
    """

    fpk_lib = {}
    sum_fpk = 0

    with open(f"{input_dir}{input_file}", 'r') as counts_in:
        name = input_file.strip().split(".")[0]
        for line in counts_in:
            if line.startswith("#"):
                continue
            elif line.startswith("Geneid"):
                continue
            else:
                count = int(line.strip().split("\t")[6])
                length = int(line.strip().split("\t")[5])
                fpk = count/length
                fpk_lib.update({line.strip().split("\t")[0]: fpk})
                sum_fpk += fpk

    counts_in.close()

    with open(f"{output_dir}{name}_tpm_log.txt", 'w') as tpm_out:
        with open(f"{input_dir}{input_file}", 'r') as counts_in_write:
            for line_write in counts_in_write:
                if line_write.startswith("#"):
                    continue
                elif line_write.startswith("Geneid"):
                    tpm_out.write(line_write)
                else:
                    tpm_log = math.log2((10**6 * (fpk_lib[line_write.strip().split("\t")[0]]/sum_fpk)) + 1)
                    tpm_out.write(f"{line_write.strip().split()[0]}\t{line_write.strip().split()[1]}\t"
                                  f"{line_write.strip().split()[2]}\t{line_write.strip().split()[3]}\t"
                                  f"{line_write.strip().split()[4]}\t{line_write.strip().split()[5]}\t"
                                  f"{tpm_log}\n")
        tpm_out.close()
    counts_in_write.close()

    return


def main():
    """Main function that runs all other functions. It is set up to run the calculating_tpm function in parallel"""
    user_input = catch_input()

    files = [f for f in listdir(user_input[0]) if isfile(join(user_input[0], f))]

    with Pool(4) as pool:
        run_calculating_tpm = pool.starmap(calculating_tpm, zip(repeat(user_input[0]), files, repeat(user_input[1])))

    return

# Main


if __name__ == '__main__':
    main()
