#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs feature counts on the mapped reads for the cocultures
"""

# Imports
import re
import sys
import subprocess

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the mapped reads are stored, the directory in which the featureCounts output needs to
    be stored, and the file with the metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the SAM files, the directory in which to store the output,"
              " and the file with the metadata")
        sys.exit(1)


def read_meta_data(file_meta_data):
    """Function that reads the metadata file

    file_meta_data: name of the metadata file

    output: list of lines in which no underscore is present in the identfier names anymore
    """
    file_1 = open(file_meta_data, 'r')
    raw_lines = file_1.readlines()
    lines = []
    for line in raw_lines:
        line = line.strip()
        lines.append(line.split(","))

    clean_lines = clean_identifier(lines)
    return clean_lines


def clean_identifier(lines):
    """Function that removes the underscore from the identifiers in the metadata

    lines: list of lines from the metadata file

    output: list of lines from the metadata without underscore
    """
    for i in range(len(lines)):
        if re.match(rf".*_.*", lines[i][0]):
            identifier = re.split(rf"_", lines[i][0])[0]
            lines[i][0] = identifier

    return lines


def running_feature_counts(clean_lines, input_directory, output_directory):
    """Function that runs featureCounts

    clean_lines: metadata lines without underscores
    input_directory: the directory in which all the mapped reads are stored
    output_directory: the directory to which to write the featureCounts output
    """
    for i in range(len(clean_lines)):
        if len(clean_lines[i]) > 4:
            cmd = "featureCounts -p -B -C -P --fracOverlap 0.5 -T 6 {} -a GTF/{}.gtf -t CDS -g locus_tag " \
                  "-o {}{}_counts.txt {}{}_sorted.sam".format(clean_lines[i][4], clean_lines[i][2], output_directory,
                                                              clean_lines[i][0], input_directory, clean_lines[i][0])
            run = subprocess.run(cmd, shell=True)
        else:
            cmd = "featureCounts --fracOverlap 0.5 -T 6 {} -a GTF/{}.gtf -t CDS -g locus_tag -o " \
                  "{}{}_counts.txt {}{}_sorted.sam".format(clean_lines[i][3], clean_lines[i][1], output_directory,
                                                           clean_lines[i][0], input_directory, clean_lines[i][0])
            run = subprocess.run(cmd, shell=True)

    return

# Main


if __name__ == "__main__":
    user_input = catch_input()
    metadata = read_meta_data(user_input[2])
    running_feature_counts(metadata, user_input[0], user_input[1])
