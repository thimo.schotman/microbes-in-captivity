# microbes in captivity

Welcome to the git page of my MSc thesis project titled 'Microbes in captivity'. In this git you will find all the necessary files to recreate my thesis project, or
do a similar project of your own.

You will find all the necessary scripts to run the pipeline in the 'scripts_pipeline' folder. All the scripts I used to do my statistical analyses can be found
in the 'scripts_analysis' folder. Mind you, a lot of these scripts have hardcoded file paths in them, referring to e.g. the count_tables folder. Make sure to check
if these file paths suit your application.

In the 'metafiles' folder you will find all the files containing metadata that are used in the pipeline. Furthermore, it contains files with useful information
such as the links to the ftp sites from which I retrieved my genomes (based on Tantoso et al. (2022)), and the PRECISE 2.0 (Lamoureux et al. (in press)) metadata.
It is important to note that the scripts in the pipeline use metafiles. You will find examples of these metafiles in this folder as well. Please be careful when 
creating these files yourself. The parsing is done based on length, so your metafile should be constructed as can be seen for each class of data (e.g. coculture or
monoculture) in this folder. Initially it should be constructed without the read direction included, once the read direction has been calculated, it should be 
included in the metafile. Once again, it is vital to do this in this particular way, as the provided scripts use length based indexing, and as such, things will go
wrong if you do not do this. Consider yourself warned.

The count_table folders contain the count tables produced in this research. These are useful should you wish to replicate the research I have done.

Feel free to use and adjust the files in this git however you see fit, but don't forget to cite where necessary.


REFERENCES

Lamoureux, C. R., Decker, K. T., Sastry, A. V., McConn, J. L., Gao, Y., & Palsson, B. O. (in press). PRECISE 2.0 - an expanded high-quality RNA-seq compendium for
    Escherichia coli K-12 reveals high-resolution transcriptional regulatory structure. BioRxiv. https://doi.org/10.1101/2021.04.08.439047 

Tantoso, E., Eisenhaber, B., Kirsch, M., Shitov, V. A., Zhao, Z., & Eisenhaber, F. (2022). To kill or to be killed: pangenome analysis of Escherichia coli strains
    reveals a tailocin specific for pandemic ST131. BMC Biology, 20(1). https://doi.org/10.1186/s12915-022-01347-7
