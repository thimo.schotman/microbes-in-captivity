#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that downloads all the genomes and their corresponding annotations for the pangenome
"""

# Imports
import sys
import subprocess
from multiprocessing import Pool
from itertools import repeat

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the .fna files will be stored, a directory where all the .gff files will be stored,
    and the name of the file with all the ftp links.
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3])]
        return input_dirs
    except IndexError:
        print("Please give in the directory to store the fna files, the directory to store the "
              "GFF files, and the file with the ftp links")
        sys.exit(1)


def extracting_ftp_links(ftp_file):
    """Function that extracts all the ftp links from the file with the links

    ftp_file: name of the file which has all the ftp links in it

    output: list of ftp links
    """
    ftp_links = []
    with open(ftp_file, 'r') as input_file:
        for line in input_file:
            ftp_links.append(line.strip().split('\t')[-1])
    input_file.close()

    return ftp_links


def download_ftp(ftp_link, directory_fna, directory_gff):
    """Function that downloads all the files based on the ftp links

    ftp_link: the link from which to download the files
    directory_fna: the directory in which to store the .fna files
    directory_gff: the directory in which to store the .gff files
    """
    gcf_name = ftp_link.split("/")[-1]
    cmd_fna = "wget {}/{}_genomic.fna.gz -P {}".format(ftp_link, gcf_name, directory_fna)
    subprocess.run(cmd_fna, shell=True)

    cmd_gff = "wget {}/{}_genomic.gff.gz -P {}".format(ftp_link, gcf_name, directory_gff)
    subprocess.run(cmd_gff, shell=True)

    return


def main():
    """Main function that runs all the other functions. It runs download_ftp in parallel"""
    user_input = catch_input()
    ftp_list = extracting_ftp_links(user_input[2])

    with Pool(3) as pool:
        results = pool.starmap(download_ftp, zip(ftp_list, repeat(user_input[0]), repeat(user_input[1])))

    return

# Main


if __name__ == '__main__':
    main()
