#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that filters count tables so only genes present in the homology grouping are in the count tables
"""

# Import
import sys
import re
from os import listdir
from os.path import isfile, join

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory where all the count files are stored, and the pantools homology list
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory where all the count files are stored, and the pantools homology list")
        sys.exit(1)


def creating_homology_set(homology_file):
    """Function that creates a set of mg1655 identifiers that are present in the homology groups

    homology_file: file with the homology grouping

    output: a set with mg1655 identifiers that are present in the homology grouping
    """
    homology_set = set()
    with open(homology_file, 'r') as homology_input:
        for line in homology_input:
            if re.search(rf"b[0-9]{4}", line):
                entries = line.strip().split()
                for entry in entries:
                    if re.search(rf"b[0-9]{4}", entry):
                        identifier = entry.split("-")[1]
                        homology_set.add(identifier)
                    else:
                        continue
            else:
                continue
        homology_input.close()

    return homology_set


def filtering_count_tables(homology_set, count_table, input_dir):
    """Function that filters the count tables

    homology_set: the set of mg1655 identifiers that are present in the homology groups
    count_table: the count table that will be filtered by the function
    input_dir: the directory where all the count table are stored
    """
    table_path = f"{input_dir}{count_table}"
    table_out_path = f"{input_dir}filtered_{count_table}"
    with open(table_path, 'r') as table_in:
        with open(table_out_path, 'w') as table_out:
            for line in table_in:
                if line.startswith("Geneid"):
                    table_out.write(line)
                elif line.split(",")[0] in homology_set:
                    table_out.write(line)
                else:
                    continue
        table_out.close()
    table_in.close()

    return


def main():
    """Main function that executes all the functions. It runs the filtering_count_tables for all count table files
    in a single folder"""
    user_input = catch_input()
    files = [f for f in listdir(user_input[0]) if isfile(join(user_input[0], f))]
    pan_homology_set = creating_homology_set(user_input[1])

    for file in files:
        filtering_count_tables(pan_homology_set, file, user_input[0])

    return

# Main


if __name__ == '__main__':
    main()
