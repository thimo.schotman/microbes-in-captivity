#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that creates a count table from the count files produced by featureCounts. It filters all genes in such a way
that only those are reported that are both in PRECISE and in the count files produced (this is necessary due because
otherwise zeros are introduced due to version differences in the annotations used)
"""

# Imports
import sys
from os import listdir
from os.path import isfile, join

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the count files are stored, the name of the precise count file, and the name for the
    ouput file in which the count table will be stored
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3])]
        return input_dirs
    except IndexError:
        print("Please give in the main directory where all the count files are stored, the precise count file,"
              " and the name for the output file (with extension)")
        sys.exit(1)


def extracting_precise_identifiers(precise_count_file):
    """Function that takes the precise count file and extracts the counts and the identifiers

    precise_count_file: the name of the precise count file

    output:
        identifiers_counts: a dictionary that contains the identifiers from PRECISE as keys and the corresponding counts
        as values
        identifiers_list: a list of identifiers present in PRECISE
    """
    identifiers_counts = {}
    identifiers_list = []
    with open(precise_count_file, 'r') as precise_in:
        for line in precise_in:
            identifiers_counts.update({line.strip().split(",")[0]: ""})
            identifiers_list.append(line.strip().split(",")[0])

    return identifiers_counts, identifiers_list


def creating_count_file_library(count_file):
    """Function that creates a dictionary from the count files produced in the pipeline

    count_file: the count_file from which to create a dictionary

    output: a dictionary with the identifiers as keys and the corresponding count value as values
    """
    count_file_library = {}
    with open(count_file, 'r') as count_input:
        for line in count_input:
            if line.startswith("Geneid"):
                continue
            elif line.startswith("#"):
                continue
            else:
                identifier = line.strip().split("\t")[0]
                value = line.strip().split("\t")[6]
                count_file_library.update({identifier: value})

    return count_file_library


def writing_output(output_name, final_identifiers_counts):
    """Function that writes the final count table to a file

    output_name: name to be given to the output file
    final_identifiers_counts: dictionary with the final counts
    """
    with open(output_name, 'w') as counts_table:
        for entry in final_identifiers_counts:
            if final_identifiers_counts[entry] == "":
                continue
            else:
                counts_table.write(f"{entry}{final_identifiers_counts[entry]}\n")

    return


def creating_count_table(input_dir, identifiers_counts, identifiers_list, output_name):
    """Function that creates the count tables.

    input_dir: the directory in which to find the count files
    identifiers_counts: dictionary containing the identifiers and counts from PRECISE
    identifiers_list: list of identifiers from PRECISE
    output_name: name to be given to the output file
    """
    files = [f for f in listdir(input_dir) if isfile(join(input_dir, f))]
    files.sort()
    for file in files:
        print(f"working on the following file:{file}")
        path_file = f"{input_dir}{file}"
        counts_from_file = creating_count_file_library(path_file)
        name = file.split("_")[0]

        for entry in identifiers_list:
            if entry == "Geneid":
                identifiers = f"{identifiers_counts[entry]},{name}"
                identifiers_counts.update({entry: identifiers})
            elif entry in counts_from_file:
                added_value = f"{identifiers_counts[entry]},{counts_from_file[entry]}"
                identifiers_counts.update({entry: added_value})
            else:
                continue

    writing_output(output_name, identifiers_counts)

    return


def main():
    """Main function that runs all other functions"""
    user_input = catch_input()
    identifiers_precise_lib, identifiers_precise_list = extracting_precise_identifiers(user_input[1])
    creating_count_table(user_input[0], identifiers_precise_lib, identifiers_precise_list, user_input[2])

    return

# Main


if __name__ == '__main__':
    main()
