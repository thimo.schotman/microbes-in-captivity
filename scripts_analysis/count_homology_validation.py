import re
import sys
from os import listdir
from os.path import isfile, isdir, join
from multiprocessing import Pool
from itertools import repeat
import subprocess


def catch_input():
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the main directory where all the metatranscriptomic data is stored, and the pantools"
              "homology file")
        sys.exit(1)


def creating_mg1655_dict(input_dir):
    mg1655_dict = {}
    with open(f"{input_dir}1_counts.txt", 'r') as dict_input:
        for line in dict_input:
            if line.startswith("#"):
                continue
            elif line.startswith("Geneid"):
                continue
            else:
                identifier = line.strip().split("\t")[0]
                value = int(line.strip().split("\t")[6])
                mg1655_dict.update({identifier: value})

    return mg1655_dict


def creating_homology_dict(homology_file):
    homology_dict = {}
    with open(homology_file, 'r') as homology_input:
        for line in homology_input:
            if re.search(rf"b[0-9]{4}", line):
                header = True
                entries_clean = []
                entries = line.strip().split()
                for entry in entries:
                    if header:
                        header = False
                    elif re.search(rf"b[0-9]{4}", entry):
                        identifier = entry.split("-")[1]
                        entries.remove(entry)
                    else:
                        entry_clean = entry.split("-")[1]
                        entries_clean.append(entry_clean)
                homology_dict.update({identifier: entries_clean})
            else:
                continue
        homology_input.close()

    return homology_dict


def count_homology_converter(input_dir, main_dir, homology_dict):
    count_path = f"{main_dir}{input_dir}/counts/counts/"
    files = [f for f in listdir(count_path) if isfile(join(count_path, f))]
    mg1655_dict = creating_mg1655_dict(count_path)
    for file in files:
        if file == "1_counts.txt":
            continue
        else:
            with open(f"{count_path}{file}", 'r') as count_input:
                for line in count_input:
                    if line.startswith("#"):
                        continue
                    elif line.startswith("Geneid"):
                        continue
                    elif line.split("\t")[6] == 0:
                        continue
                    else:
                        identifier = line.strip().split("\t")[0]
                        for entry in homology_dict:
                            if identifier in homology_dict[entry]:
                                value = mg1655_dict[entry] + int(line.strip().split("\t")[6])
                                mg1655_dict.update({entry: value})
                            else:
                                continue
    start_mg1655_file = f"{count_path}1_counts.txt"
    srr = input_dir
    writing_output(main_dir, srr, start_mg1655_file, mg1655_dict)

    return mg1655_dict


def writing_output(main_dir, srr, start_mg1655_file, updated_mg1655_dict):
    with open(f"{main_dir}/homology_counts/{srr}_counts_homology.txt", 'w') as homology_counts:
        with open(start_mg1655_file, 'r') as template:
            for line in template:
                if line.startswith("#"):
                    continue
                elif line.startswith("Geneid"):
                    homology_counts.write(line)
                else:
                    info = line.split("\t")
                    value = updated_mg1655_dict[info[0]]
                    homology_counts.write(f"{info[0]}\t{info[1]}\t{info[2]}\t{info[3]}\t{info[4]}\t{info[5]}\t"
                                          f"{value}\n")

    return


def main():
    user_input = catch_input()
    homology_dictionary = creating_homology_dict(user_input[1])
    srr_folders = [f for f in listdir(user_input[0]) if isdir(join(user_input[0], f))]

    cmd_homology_count_folder = f"mkdir {user_input[0]}homology_counts"
    create_homology_count_folder = subprocess.check_call(cmd_homology_count_folder, shell=True)

    with Pool(6) as pool:
        run_homology_converter = pool.starmap(count_homology_converter, zip(srr_folders, repeat(user_input[0]),
                                                                            repeat(homology_dictionary)))

    return


if __name__ == '__main__':
    main()
