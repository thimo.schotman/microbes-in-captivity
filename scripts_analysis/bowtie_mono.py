#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs bowtie for the monocultures
"""

# Imports
import sys
from os import listdir
from os.path import isfile, join
import subprocess
import re


def catch_input():
    """Catches the user input from the command line

    returns a directory with the working directory for the monocultures and the name of the file with the metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the working directory for the mono-cultures you want to process, "
              "and the file with the metadata")
        sys.exit(1)


def replace_identifier_for_file(meta_file, working_dir):
    """Function that replaces the identifier in the metafile with the appropriate filename

    meta_file: the name of the meta_file
    working_dir: the name of the working directory

    output: list of line from the metadata
    """
    path = f"{working_dir}/trimmed_reads"
    files = [f for f in listdir(path) if isfile(join(path, f))]
    lines_metadata = []
    with open(meta_file, 'r') as meta_data:
        for line in meta_data:
            meta_line = line.strip().split(",")
            if len(meta_line) > 2:
                for j in range(len(files)):
                    file_1 = re.findall(rf"{meta_line[0]}[._].*", files[j])
                    file_2 = re.findall(rf"{meta_line[1]}[._].*", files[j])
                    if len(file_1) > 0:
                        meta_line[0] = (file_1[0])
                    elif len(file_2) > 0:
                        meta_line[1] = (file_2[0])
                lines_metadata.append(meta_line)
            else:
                for j in range(len(files)):
                    file_1 = re.findall(rf"{meta_line[0]}[._].*", files[j])
                    if len(file_1) > 0:
                        meta_line[0] = (file_1[0])
                lines_metadata.append(meta_line)

    return lines_metadata


def running_bowtie(lines_metadata, working_dir):
    """Function that runs bowtie for the monocultures

    lines_metadata: list of lines from the metadata
    working_dir: name of the working directory
    """
    path_input = f"{working_dir}/trimmed_reads/"
    path_output = f"{working_dir}/mapped_reads/"
    path_stats = f"{working_dir}/stats_mapping/"

    cmd_output_path = f"mkdir {path_output}"
    output_path_create = subprocess.check_call(cmd_output_path, shell=True)

    cmd_stats_path = f"mkdir {path_stats}"
    stats_path_create = subprocess.check_call(cmd_stats_path, shell=True)

    for line in lines_metadata:
        identifier = re.split(rf"[._]", line[0])[0]
        reference = line[-1]
        if len(line) > 2:
            cmd_bowtie_paired = f"bowtie -X 1000 -3 3 -n 2 --threads 10 -S --no-unal -x {reference} -1 " \
                                f"{path_input}{line[0]} -2 {path_input}{line[1]} 1> {path_output}{identifier}.sam 2> " \
                                f"{path_stats}{identifier}_bowtie.txt"

            subprocess.check_call(cmd_bowtie_paired, shell=True)
        else:
            cmd_bowtie_unpaired = f"bowtie -X 1000 -3 3 -n 2 --threads 10 -S --no-unal -x {reference} " \
                                  f"{path_input}{line[0]} 1> {path_output}{identifier}.sam 2> " \
                                  f"{path_stats}{identifier}_bowtie.txt"
            subprocess.run(cmd_bowtie_unpaired, shell=True)

    return


def main():
    """The main function to run all other functions. Nothing special to note
    """
    user_input = catch_input()
    meta_lines = replace_identifier_for_file(user_input[1], user_input[0])
    running_bowtie(meta_lines, user_input[0])

    return

# Main


if __name__ == '__main__':
    main()
