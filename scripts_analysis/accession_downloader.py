#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that downloads accessions from a file with accessions
"""

# Imports
import subprocess
import sys
from multiprocessing import Pool
from itertools import repeat

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where FastQ files are to be stored and a file with the accessions
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory to store the fastq files, and the file with the accessions")
        sys.exit(1)


def getting_accession(accession_file):
    """Reads the accessions from the file with accessions.

    accession_file: csv file with the accessions that need to be downloaded

    returns a list of accessions that need to downloaded
    """
    accessions = []
    with open(accession_file, 'r') as accession_input:
        for line in accession_input:
            accessions.append(line.strip().split(",")[0])

    return accessions


def run_download_loop(accession, store_path):
    """Function that downloads a accession

    accession: which accession to run the download command for
    store_path: the file path in which to store the downloaded accession
    """

    cmd = f'fasterq-dump {accession} -O {store_path}'
    run = subprocess.check_call(cmd, shell=True)

    return


def main():
    """The main function that runs all the other functions. The download loop is ran in parallel

    """
    user_input = catch_input()
    accession_list = getting_accession(user_input[1])

    with Pool(3) as pool:
        results = pool.starmap(run_download_loop, zip(accession_list, repeat(user_input[0])))

    return

# Main


if __name__ == "__main__":
    main()
