#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that checks for reads that have crossmapped and removes them
"""

# Imports
import re
import sys
from multiprocessing import Pool
from itertools import repeat

# Functions


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the files with mapped reads are stored, a directory where all the filtered files
    need to be stored, a directory with all the stats files from Bowtie, and the name of the metadata file
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3]), str(sys.argv[4])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the mapped files, the directory to store the "
              "files, the directory with the stats files, and the file with the metadata")
        sys.exit(1)


def read_meta_data(file_meta_data):
    """Function that reads the metadata file

    file_meta_data: the metadata file to be read

    ouput: a list of lines in which the identifiers do not contain an underscore anymore
    """
    file_1 = open(file_meta_data, 'r')
    raw_lines = file_1.readlines()
    lines = []
    for line in raw_lines:
        line = line.strip()
        lines.append(line.split(","))

    clean_lines = clean_identifier(lines)

    return clean_lines


def clean_identifier(lines):
    """Function that cleans the metadata lines, remove the underscore in the accession names

    lines: a list of lines from the original metadata file

    output: a list of clean lines, without underscores in the identifiers
    """
    for i in range(len(lines)):
        if re.match(rf".*_.*", lines[i][0]):
            identifier = re.split(rf"_", lines[i][0])[0]
            lines[i][0] = identifier

    return lines


def extract_identifiers(input_directory, metadata_line):
    """Function that takes the metadata lines and extracts the identifiers for each line

    input_directory: the directory where all the mapped reads are stored
    metadata_line: the lines from the original metadata file

    output: a set of identifiers found in the files in the input directory
    """
    if len(metadata_line) > 3:
        sam_file = "{}/{}_{}.sam".format(input_directory, metadata_line[0], metadata_line[3])
    else:
        sam_file = "{}/{}_{}.sam".format(input_directory, metadata_line[0], metadata_line[2])
    identifiers = set()
    with open(sam_file, "r") as file_input:
        for line in file_input:
            if len(line.split("\t")) > 4:
                identifiers.add(line.split("\t")[0])
            else:
                continue

    return identifiers


def comparing_sam_files(sam_input, sam_output, input_directory, metadata, stats_directory):
    """"Function that compares the sam files and removes those reads that have crossmapped

    sam_input: the input filename
    sam_output: the output filename
    input_directory: the input_directory where all the SAM files are stored
    metadata: the metadata
    stats_directory: the directory where all the stats files have been stored
    """
    removed_lines = 0
    file = sam_input.split("/")[-1]
    accession = file.split(".")[0]

    for line in metadata:
        if line[0] == accession:
            set_identifiers = extract_identifiers(input_directory, line)
        else:
            continue

    with open(sam_input, 'r') as file_input:
        with open(sam_output, 'w') as file_output:
            for line in file_input:
                if len(line.split("\t")) > 4:
                    if line.split("\t")[0] in set_identifiers:
                        removed_lines += 1
                        continue
                    else:
                        file_output.write(line)
                else:
                    file_output.write(line)
    writing_stats(removed_lines, stats_directory, accession)
    file_input.close()
    file_output.close()

    return


def writing_stats(stats, stats_directory, accession):
    """Function that writes the cross-mapping statistics to the original bowtie statistics file

    stats: the number of cross-mapped reads
    stats_directory: the directory where the stats files are stored
    accession: the accession for which to write the statistics
    """
    file_name = "{}{}_bowtie.txt".format(stats_directory, accession)
    with open(file_name, 'a') as stats_file:
        stats_file.write("\nIn total, %d reads crossmapped and have been removed\n" % stats)

    stats_file.close()

    return


def main():
    """"Main function that also creates names for all the input and output files. This so the comparing_sam_files
    function can be ran parallel
    """
    user_input = catch_input()
    metadata_lists = read_meta_data(user_input[3])
    input_files = []
    output_files = []

    for i in range(len(metadata_lists)):
        input_file = "{}{}.sam".format(user_input[0], metadata_lists[i][0])
        input_files.append(input_file)
        output_file = "{}{}_noCross.sam".format(user_input[1], metadata_lists[i][0])
        output_files.append(output_file)

    with Pool(4) as pool:
        results = pool.starmap(comparing_sam_files, zip(input_files, output_files, repeat(user_input[0]),
                                                        repeat(metadata_lists), repeat(user_input[2])))

# Main


if __name__ == "__main__":
    main()
