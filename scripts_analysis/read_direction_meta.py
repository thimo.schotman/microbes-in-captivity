#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs RSEQC to infer the read direction of the experiments, specifically for the metatranscriptomic
experiments
"""

# Imports
import sys
import subprocess
from os import listdir
from os.path import isfile, isdir, join
from multiprocessing import Pool
from itertools import repeat

# Fucntions


def catch_input():
    """Catches the user input from the command line

    returns the directory in which the mapped reads can be found, the directory with the .bed files, and the pantools
    genome list
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3])]
        return input_dirs
    except IndexError:
        print("Please give in the directory in which to find the reads and store the results, the directory with the "
              "bed files, and the pantools genome list")
        sys.exit(1)


def sorting_sam_files(srr_dir, main_dir):
    """Function that sorts the .sam files for further processing in the infer_experiment script

    srr_dir: the specific srr directory for which to sort the files
    main_dir: the main directory in which all the metatranscriptomic data is stored
    """
    cmd_folder = f"mkdir {main_dir}{srr_dir}/sorted_reads"
    run_folder = subprocess.check_call(cmd_folder, shell=True)

    path_mapped = f"{main_dir}{srr_dir}/mapped_reads/"
    path_sorted = f"{main_dir}{srr_dir}/sorted_reads/"
    files = [f for f in listdir(path_mapped) if isfile(join(path_mapped, f))]
    for file in files:
        file_name = file.strip().split(".")[0]
        cmd_sorted = f"samtools sort {path_mapped}{file} > {path_sorted}{file_name}_sorted.sam"
        run_sorted = subprocess.check_call(cmd_sorted, shell=True)

    return


def running_infer_experiment(srr_dir, main_dir, dir_bed, pantools_file):
    """Function that runs the infer_experiment script

    srr_dir: the specific srr directory for which to run the script
    main_dir: the main directory in which all the metatranscriptomic data is stored
    dir_bed: the directory with the bed files
    pantools_file: the pantools genome list
    """
    number_name = {}

    with open(pantools_file) as name_number_input:
        for line in name_number_input:
            number = line.strip().split()[0]
            name = line.strip().split("/")[-1].split("_genomic.gff")[0]
            number_name.update({number: name})

    cmd_infer_folder = f"mkdir {main_dir}{srr_dir}/read_direction"
    run_infer_folder = subprocess.check_call(cmd_infer_folder, shell=True)

    path_infer = f"{main_dir}{srr_dir}/read_direction/"
    path_sorted = f"{main_dir}{srr_dir}/sorted_reads/"
    files = [f for f in listdir(path_sorted) if isfile(join(path_sorted, f))]

    for file in files:
        file_number = file.strip().split("_")[1].split(".")[0]

        cmd = f"infer_experiment.py -r {dir_bed}{number_name[file_number]}.gff.bed " \
              f"-i {path_sorted}{file} > {path_infer}{file_number}_read_direction.txt"
        run = subprocess.check_call(cmd, shell=True)

    return


def creating_summary(srr_dir, main_dir):
    """Function that creates a summary for a specific accession directory what the results of the infer_experiment are

    srr_dir: the specific srr directory for which to make a summary
    main_dir: the main directory in which all the metatranscriptomic data is stored
    """
    path_infer = f"{main_dir}{srr_dir}/read_direction/"
    files = [f for f in listdir(path_infer) if isfile(join(path_infer, f))]

    with open(f"{path_infer}{srr_dir}_direction_stats.txt", 'w') as read_direction_stats:
        for file in files:
            counter = file.strip().split("_")[0]

            with open(f"{path_infer}{file}", 'r') as read_direction:
                lines = read_direction.readlines()
                if lines[0] == "Unknown Data type\n":
                    read_direction_stats.write(f"{counter}, unknown\n")
                else:
                    dir_1 = float(lines[4].split(":")[-1])
                    dir_2 = float(lines[5].split(":")[-1])

                    if dir_1 > 2 * dir_2:
                        read_direction_stats.write(f"{counter}, {dir_1}, -s 1\n")
                    elif dir_2 > 2 * dir_1:
                        read_direction_stats.write(f"{counter}, {dir_2}, -s 2\n")
                    else:
                        read_direction_stats.write(f"{counter}, {dir_1}, -s 0\n")

            read_direction.close()
    read_direction_stats.close()

    return


def main():
    """The main function that runs all other functions. It is set up to run the several functions in parallel"""
    user_input = catch_input()

    srr_folders = [f for f in listdir(user_input[0]) if isdir(join(user_input[0], f))]
    srr_folders.remove('unmapped')
    srr_folders.remove('summaries')
    srr_folders.remove("all_read_directions")

    with Pool(6) as pool:
        run_sorting = pool.starmap(sorting_sam_files, zip(srr_folders, repeat(user_input[0])))
        run_infer = pool.starmap(running_infer_experiment, zip(srr_folders, repeat(user_input[0]),
                                                               repeat(user_input[1]), repeat(user_input[2])))
        run_summary = pool.starmap(creating_summary, zip(srr_folders, repeat(user_input[0])))

    return

# Main


if __name__ == '__main__':
    main()
