#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs bowtie for the cocultures
"""

# Imports
import sys
from os import listdir
from os.path import isfile, join
import subprocess
import re


def catch_input():
    """Catches the user input from the command line

    returns a directory where all the trimmed files are stored, a directory where all the files with the mapped reads
    will be stored, and the name of the metadata file.
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3])]
        return input_dirs
    except IndexError:
        print("Please give in the directory with the trimmed files, the directory to store the mapped "
              "files, and the file with the metadata")
        sys.exit(1)


def input_files(input_dir):
    """Function that returns all files in a specific directory

    input_dir: The directory from which the files are to be extracted

    output: A list of files in the input directory
    """
    files = [f for f in listdir(input_dir) if isfile(join(input_dir, f))]
    return files


def read_meta_data(file_meta_data):
    """Function that reads the metadata from the metatdata file

    file_meta_data: The file with the metadata

    output: list of lines in the metadata file
    """
    file_1 = open(file_meta_data, 'r')
    raw_lines = file_1.readlines()
    lines = []
    for line in raw_lines:
        line = line.strip()
        lines.append(line.split(","))

    return lines


def replace_identifier_for_file(lines, files):
    """Function that replaces all the identifiers in the metadata for their corresponding filenames

    lines: The lines from the metadata
    files: The files that contain the trimmed reads

    ouput: list of lines, but with filenames instead of identifiers
    """
    for i in range(len(lines)):
        if len(lines[i]) > 4:
            for j in range(len(files)):
                file_1 = re.findall(rf"{lines[i][0]}[._].*", files[j])
                file_2 = re.findall(rf"{lines[i][1]}[._].*", files[j])
                if len(file_1) > 0:
                    lines[i][0] = (file_1[0])
                elif len(file_2) > 0:
                    lines[i][1] = (file_2[0])
        else:
            for j in range(len(files)):
                file_1 = re.findall(rf"{lines[i][0]}[._].*", files[j])
                if len(file_1) > 0:
                    lines[i][0] = (file_1[0])

    return lines


def running_bowtie_coli(lines, input_dir, output_dir):
    """Function that runs bowtie with an e. coli reference

    lines: lines from the metadata
    input_dir: the directory with all the trimmed reads
    output_dir: the directory to store the mapped reads
    """
    for i in range(len(lines)):
        identifier = re.split(rf"[._]", lines[i][0])[0]
        if len(lines[i]) > 4:
            cmd = "bowtie -X 1000 -3 3 -n 2 --threads 10 -S --no-unal -x {} -1 {}{} -2 {}{} 1> {}{}.sam 2> " \
                  "{}{}_bowtie.txt".format(lines[i][2], input_dir, lines[i][0], input_dir, lines[i][1], output_dir,
                                           identifier, output_dir, identifier)
            subprocess.run(cmd, shell=True)
        else:
            cmd = "bowtie -X 1000 -3 3 -n 2 --threads 10 -S --no-unal -x {} {}{} 1> {}{}.sam 2> {}{}_bowtie.txt".format(
                lines[i][1], input_dir, lines[i][0], output_dir, identifier, output_dir,
                identifier)
            subprocess.run(cmd, shell=True)

    return


def running_bowtie_cross_mapping(lines, input_dir, output_dir):
    """Function that runs bowtie with the reference for the species e. coli was cocultured with

    lines: lines from the metadata
    input_dir: the directory with all the files with trimmed reads
    output_dir: the directory where all the mapped reads are to be stored
    """
    for i in range(len(lines)):
        identifier = re.split(rf"[._]", lines[i][0])[0]
        if len(lines[i]) > 4:
            cmd = "bowtie -X 1000 -3 3 -n 2 --threads 10 -S --no-unal -x {} -1 {}{} -2 {}{} 1> {}{}_{}.sam 2> " \
                  "{}{}_{}_bowtie.txt".format(lines[i][3], input_dir, lines[i][0], input_dir, lines[i][1], output_dir,
                                              identifier, lines[i][3], output_dir, identifier, lines[i][3])
            subprocess.run(cmd, shell=True)
        else:
            cmd = "bowtie -X 1000 -3 3 -n 2 --threads 10 -S --no-unal -x {} {}{} 1> {}{}_{}.sam 2> {}{}_{}_bowtie.txt".\
                format(lines[i][2], input_dir, lines[i][0], output_dir, identifier,
                       lines[i][2], output_dir, identifier, lines[i][2])
            subprocess.run(cmd, shell=True)

    return

# Main


if __name__ == "__main__":
    user_input = catch_input()
    list_input_files = input_files(user_input[0])
    meta_data = read_meta_data(user_input[2])
    clean_meta = replace_identifier_for_file(meta_data, list_input_files)
    running_bowtie_coli(clean_meta, user_input[0], user_input[1])
    running_bowtie_cross_mapping(clean_meta, user_input[0], user_input[1])
