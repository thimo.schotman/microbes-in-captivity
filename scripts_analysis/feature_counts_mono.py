#!/usr/bin/env python3
"""
Author: Thimo Schotman
Script that runs featureCounts for the monocultures. It does the same as the script for the cocultures,
but this script takes care of directories and subdirectories internally
"""

# Imports
import re
import sys
import subprocess

# Functions


def catch_input():
    """Catches the user input from the command line

    returns the directory in which all the monoculture data is stored, and the name of file with the metadata
    """
    try:
        input_dirs = [str(sys.argv[1]), str(sys.argv[2])]
        return input_dirs
    except IndexError:
        print("Please give in the directory in which the data of the experiment is stored, "
              "and the file with the metadata")
        sys.exit(1)


def read_meta_data(file_meta_data):
    """Function that reads the metadata file

    file_meta_data: the name of the metadata file

    output: list of lines from the metadata with the underscore removed in the accession names
    """
    file_1 = open(file_meta_data, 'r')
    raw_lines = file_1.readlines()
    lines = []
    for line in raw_lines:
        line = line.strip()
        lines.append(line.split(","))

    clean_lines = clean_identifier(lines)
    return clean_lines


def clean_identifier(lines):
    """Function that removes the underscore from the accession name

    lines: list with lines from the metadata file

    output: list of line from the metadata file without underscores in the identifiers
    """
    for i in range(len(lines)):
        if re.match(rf".*_.*", lines[i][0]):
            identifier = re.split(rf"_", lines[i][0])[0]
            lines[i][0] = identifier

    return lines


def running_feature_counts(clean_lines, main_dir):
    """Function that runs featureCounts for the metatranscriptomic data

    clean_lines: list of lines from the metadata without the underscore in the identifiers
    main_dir: the main directory where all the information of the monocultures is stored
    """
    input_dir = f"{main_dir}mapped_reads/"
    output_dir = f"{main_dir}counts/"
    cmd_output_dir = f"mkdir {output_dir}"
    make_output_dir = subprocess.check_call(cmd_output_dir, shell=True)

    for line in clean_lines:
        direction = line[-1]
        genome = line[-2]
        if len(line) > 3:
            cmd_paired = f"featureCounts -p -B -C -P --fracOverlap 0.5 -T 6 {direction} -a GTF/{genome}.gtf -t CDS " \
                         f"-g locus_tag -o {output_dir}{line[0]}_counts.txt {input_dir}{line[0]}_sorted.sam"
            run_paired = subprocess.check_call(cmd_paired, shell=True)
        else:
            cmd_unpaired = f"featureCounts --fracOverlap 0.5 -T 6 {direction} -a GTF/{genome}.gtf -t CDS " \
                           f"-g locus_tag -o {output_dir}{line[0]}_counts.txt {input_dir}{line[0]}_sorted.sam"
            run_unpaired = subprocess.run(cmd_unpaired, shell=True)

    return

# Main


if __name__ == "__main__":
    user_input = catch_input()
    metadata = read_meta_data(user_input[1])
    running_feature_counts(metadata, user_input[0])
